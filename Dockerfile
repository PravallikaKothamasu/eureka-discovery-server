FROM openjdk:8-jre
COPY target /eurekadiscovery
WORKDIR /eurekadiscovery
ENTRYPOINT ["java", "-jar", "discoveryserver-0.0.1-SNAPSHOT.jar"]